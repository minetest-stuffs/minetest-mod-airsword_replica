minetest.register_craft({
	output = 'airsword_replica:airsword_replica',
	recipe = {
		{'airsword_replica:air_bar', 'default:diamondblock', 'airsword_replica:air_bar'},
		{'airsword_replica:air_bar', 'default:diamondblock', 'airsword_replica:air_bar'},
		{'airsword_replica:air_bar', 'default:sword_diamond', 'airsword_replica:air_bar'},
	}
})

	minetest.register_node("airsword_replica:air_ore", {
	description = "Air ore",
	tiles = {"invisibleair.png^airsword_replica_ore.png"},
	groups = {cracky = 1},
	drawtype = "glasslike",
	drop = 'airsword_replica:packed_air',
	light_source = 6,
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_ore({
		ore_type       = "scatter",
		ore            = "airsword_replica:air_ore",
		wherein        = "air",
		clust_scarcity = 40 * 40 * 40,
		clust_num_ores = 1,
		clust_size     = 1,
		y_min          = 169,
		y_max          = 31000,
	})

minetest.register_craftitem("airsword_replica:air_bar", {
		description = "".. core.colorize("#93fbff", "Bar Of Air\n")..core.colorize("#FFFFFF", "Made of compressed air, used in making the replica of airsword."),
	inventory_image = "airsword_replica_air_bar.png",
})

minetest.register_craftitem("airsword_replica:packed_air", {
		description = "".. core.colorize("#93fbff", "Packed air\n")..core.colorize("#FFFFFF", "stacks extremelly well, Consume 100 of those to form a bar."),
	stack_max = 65535,
	on_use = function(itemstack, user, pointed_thing)
		local inv = user:get_inventory()
	if itemstack:get_count() <= 99 then
	return itemstack
	end
		itemstack:take_item(100)
		inv:add_item("main", "airsword_replica:air_bar 1")
	return itemstack
	end,
	inventory_image = "airsword_replica_packed_air.png",
})

local timer = 0
minetest.register_globalstep(function(dtime, player)
	timer = timer + dtime;
	if timer >= 0.20 then
	for _, player in pairs(minetest.get_connected_players()) do
			local controls = player:get_player_control()
			if controls.LMB then
			timer = 0
	local wielded_item = player:get_wielded_item():get_name()
		if wielded_item == "airsword_replica:airsword_replica" then
		local pos = player:getpos()
		local dir = player:get_look_dir()
		local yaw = player:get_look_yaw()
		if pos and dir and yaw then
			pos.y = pos.y + 1.6
			local obj = minetest.add_entity(pos, "airsword_replica:air_slash")
			if obj then
				minetest.sound_play("swoosh", {object=obj})
				obj:setvelocity({x=dir.x * 35, y=dir.y * 35, z=dir.z * 35})
				obj:setacceleration({x=dir.x * 0, y=0, z=dir.z * 0})
				obj:setyaw(yaw + math.pi)
				local ent = obj:get_luaentity()
				if ent then
					ent.player = ent.player or player
				end
			end
		end
	end

	end
		end
			end
				end)


local timer = 0
minetest.register_globalstep(function(dtime, player)
	timer = timer + dtime;
	if timer >= 1.0 then
	for _, player in pairs(minetest.get_connected_players()) do
	local wielded_item = player:get_wielded_item():get_name()
		if wielded_item == "airsword_replica:airsword_replica" then	
			timer = 0
			regen = player:get_hp()  
			regen = regen+1
			player:set_hp(tonumber(regen))
			air = player:get_breath()  
			air = air+1
			player:set_breath(tonumber(air))
end
end
end
	end)

minetest.register_craftitem("airsword_replica:airsword_replica", {
		description = "".. core.colorize("#93fbff", "Airsword Replica\n")..core.colorize("#FFFFFF", "Not the real thing, but the closest a mortal could get to it\n")..core.colorize("#FFFFFF", "Has 9999 melee DMG, shoots airwaves, grants regeneration and prevents suffocation."),
	inventory_image = "the_airsword_replica.png",
	wield_scale = {x=3,y=3,z=1.0},
	range = 10,
	stack_max = 1,
	tool_capabilities = {
		full_punch_interval = 0.25,
		max_drop_level=1,
		groupcaps={
			snappy={times={[1]=1.90, [2]=0.90, [3]=0.30}, uses=0, maxlevel=3},
		},
		damage_groups = {fleshy=9999},
	},
})

local airsword_replica_air_slash = {
	physical = false,
	timer = 0,
	visual = "sprite",
	visual_size = {x=3.0, y=0.0,},
	textures = {'invisibleair.png'},
	lastpos= {},
	collisionbox = {0, 0, 0, 0, 0, 0},
}
airsword_replica_air_slash.on_step = function(self, dtime)
	self.timer = self.timer + dtime
	local pos = self.object:getpos()
	local tiem = 0.002
	local node = minetest.get_node(pos)

	if self.timer > 0.20 then
		local objs = minetest.get_objects_inside_radius({x = pos.x, y = pos.y, z = pos.z}, 3)
		for k, obj in pairs(objs) do
			if obj:get_luaentity() ~= nil then
				if obj:get_luaentity().name ~= "airsword_replica:air_slash" and obj:get_luaentity().name ~= "__builtin:item" then
					local damage = 1337
					obj:punch(self.object, 1.0, {
						full_punch_interval = 1.0,
						damage_groups= {fleshy = damage},
					}, nil)
					minetest.sound_play("default_dig_cracky", {pos = self.lastpos, gain = 0.8})
				end
			else
				local damage = 1337
				obj:punch(self.object, 1.0, {
					full_punch_interval = 1.0,
					damage_groups= {fleshy = damage},
				}, nil)
				minetest.sound_play("default_dig_cracky", {pos = self.lastpos, gain = 0.8})
			end
		end
		if timer >= 0.002 + tiem then
	minetest.add_particle({
		pos = pos,
		velocity = 0,
          acceleration = {x=0, y=0, z=0},
		expirationtime = 0.2,
		size = 33,
		collisiondetection = false,
		vertical = false,
		texture = "airslash.png",
		glow = 30,
	})
		tiem = tiem + 0.002 
			end
		if self.timer >= 4.0 then
		self.object:remove()
			end

	end

	if self.lastpos.x ~= nil then
		if minetest.registered_nodes[node.name].walkable then
			if not minetest.setting_getbool("creative_mode") then
				minetest.add_item(self.lastpos, "")
			end
			minetest.sound_play("default_dig_cracky", {pos = self.lastpos, gain = 0.8})
			self.object:remove()
		end
	end
	self.lastpos= {x = pos.x, y = pos.y, z = pos.z}
end

minetest.register_entity("airsword_replica:air_slash", airsword_replica_air_slash)


